//
//  AppDelegate.h
//  PomodoroDraw
//
//  Created by Ezequiel Scaruli on 10/14/13.
//  Copyright (c) 2013 Despegar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
