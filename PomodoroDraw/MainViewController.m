//
//  MainViewController.m
//  PomodoroDraw
//
//  Created by Ezequiel Scaruli on 10/14/13.
//  Copyright (c) 2013 Despegar. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

#import "MainViewController.h"
#import "UIView+Frame.h"

#define INITIAL_DURATION        0.01
#define MAX_NUMBER              49

@interface MainViewController ()

@property (nonatomic, weak) IBOutlet UILabel *lhLabel;
@property (nonatomic, weak) IBOutlet UILabel *lLabel;
@property (nonatomic, weak) IBOutlet UILabel *cLabel;
@property (nonatomic, weak) IBOutlet UILabel *rLabel;
@property (nonatomic, weak) IBOutlet UILabel *rhLabel;

@property (nonatomic, weak) IBOutlet UIImageView *tomatoImageView;

@property (nonatomic, weak) UIButton *startButton;
@property (nonatomic, weak) UIButton *stopButton;

@property (nonatomic, weak) IBOutlet UIView *buttonsContainerView;

@property (nonatomic, assign) CGRect lhInitialFrame;
@property (nonatomic, assign) CGRect lInitialFrame;
@property (nonatomic, assign) CGRect cInitialFrame;
@property (nonatomic, assign) CGRect rInitialFrame;
@property (nonatomic, assign) CGRect rhInitialFrame;

@property (nonatomic, assign) NSInteger currentNumber;
@property (nonatomic, assign) NSInteger freeTimes;
@property (nonatomic, assign) CGFloat breakFactor;

@property (nonatomic, assign) BOOL stopped;

@end


@implementation MainViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self != nil) {
        self.currentNumber = MAX_NUMBER / 2;
    }
    
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];

    // Center label.
    self.cLabel.text = [self cLabelText];
    self.cInitialFrame = self.cLabel.frame;
    
    // Left label.
    self.lLabel.text = [self lLabelText];
    self.lLabel.transform = CGAffineTransformMakeScale(0.5, 0.5);
    self.lLabel.frameX = self.cLabel.frameX - self.lLabel.frameWidth - 20;
    self.lLabel.centerY = self.cLabel.centerY;
    self.lInitialFrame = self.lLabel.frame;
    
    // Right label.
    self.rLabel.text = [self rLabelText];
    self.rLabel.transform = CGAffineTransformMakeScale(0.5, 0.5);
    self.rLabel.frameX = self.cLabel.frameX + self.cLabel.frameWidth + 20;
    self.rLabel.centerY = self.cLabel.centerY;
    self.rInitialFrame = self.rLabel.frame;
    
    // Left hidden label.
    self.lhLabel.text = [self lhLabelText];
    self.lhLabel.transform = CGAffineTransformMakeScale(0.25, 0.25);
    self.lhLabel.frameX = self.lLabel.frameX - self.lhLabel.frameWidth - 10;
    self.lhLabel.centerY = self.cLabel.centerY;
    self.lhInitialFrame = self.lhLabel.frame;
    
    // Right hidden label.
    self.rhLabel.text = [self rhLabelText];
    self.rhLabel.transform = CGAffineTransformMakeScale(0.25, 0.25);
    self.rhLabel.frameX = self.rLabel.frameX + self.rLabel.frameWidth + 10;
    self.rhLabel.centerY = self.cLabel.centerY;
    self.rhInitialFrame = self.rhLabel.frame;
    
    // Start button
    self.startButton = [self buttonWithTitle:@"Empezar..."];
    self.startButton.frame = CGRectMake(0, 0, self.buttonsContainerView.frameWidth, self.buttonsContainerView.frameHeight);
    [self.startButton addTarget:self action:@selector(startButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [self.buttonsContainerView addSubview:self.startButton];
    
    // Stop button
    self.stopButton = [self buttonWithTitle:@"¡LISTO!"];
    self.stopButton.frame = CGRectMake(0, 0, self.buttonsContainerView.frameWidth, self.buttonsContainerView.frameHeight);
    [self.stopButton addTarget:self action:@selector(stopButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [self.buttonsContainerView addSubview:self.stopButton];
    
    [self reset];
}


- (NSString *)cLabelText {
    return [NSString stringWithFormat:@"%d", [self textNumberForNumber:self.currentNumber]];
}


- (NSString *)lLabelText {
    return [NSString stringWithFormat:@"%d", [self textNumberForNumber:self.currentNumber - 1]];
}


- (NSString *)rLabelText {
    return [NSString stringWithFormat:@"%d", [self textNumberForNumber:self.currentNumber + 1]];
}


- (NSString *)lhLabelText {
    return [NSString stringWithFormat:@"%d", [self textNumberForNumber:self.currentNumber - 2]];
}

- (NSString *)rhLabelText {
    return [NSString stringWithFormat:@"%d", [self textNumberForNumber:self.currentNumber + 2]];
}


- (NSInteger)textNumberForNumber:(NSInteger)number {
    if (number >= 0 && number <= MAX_NUMBER) {
        return number + 1;
    }
    
    if (number > MAX_NUMBER) {
        return number % MAX_NUMBER;
    }
    
    return MAX_NUMBER + number + 2;
}


- (void)startButtonAction {
    self.startButton.hidden = YES;
    self.stopButton.hidden = NO;
    
    [self performAnimationWithDuration:INITIAL_DURATION];
}


- (void)stopButtonAction {
    self.stopButton.hidden = YES;
    self.stopped = YES;
}


- (void)performAnimationWithDuration:(CGFloat)duration {
    void (^animationBlock)(void) = ^{
        self.lLabel.transform = CGAffineTransformMakeScale(0.25, 0.25);
        self.lLabel.frame = self.lhInitialFrame;
        
        self.cLabel.transform = CGAffineTransformMakeScale(0.5, 0.5);
        self.cLabel.frame = self.lInitialFrame;
        
        self.rLabel.transform = CGAffineTransformIdentity;
        self.rLabel.frame = self.cInitialFrame;
        
        self.rhLabel.transform = CGAffineTransformMakeScale(0.5, 0.5);
        self.rhLabel.frame = self.rInitialFrame;
    };
    
    void (^completionBlock)(BOOL) = ^(BOOL finished) {
        if (self.stopped) {
            // We start breaking down the movement.
        
            CGFloat newDuration = duration + [self currentBreakFactor];
            
            if (newDuration > 1.0) {
                // Stop!!!
                [self performWinnerEffect];
            } else {
                [self makeLabelsGoForward];
                [self performAnimationWithDuration:newDuration];
            }
            
        } else {
            // We keep on moving the labels.
            [self makeLabelsGoForward];
            [self performAnimationWithDuration:duration];
        }
    };
    
    [UIView animateWithDuration:duration animations:animationBlock completion:completionBlock];
}


- (void)makeLabelsGoForward {
    UILabel *lhLabelBk = self.lhLabel;
    
    self.lhLabel = self.lLabel;
    self.lLabel = self.cLabel;
    self.cLabel = self.rLabel;
    self.rLabel = self.rhLabel;
    
    self.currentNumber++;
    
    if (self.currentNumber > MAX_NUMBER) {
        self.currentNumber = 0;
    }
    
    self.rhLabel = lhLabelBk;
    self.rhLabel.frame = self.rhInitialFrame;
    self.rhLabel.text = [self rhLabelText];
}


- (void)reset {
    self.startButton.hidden = NO;
    self.stopButton.hidden = YES;
    self.breakFactor = 0.0;
    self.freeTimes = 50;
    self.stopped = NO;
}


- (CGFloat)currentBreakFactor {
    if (self.freeTimes > 0) {
        self.freeTimes--;
        return 0.0;
    }
    
    self.breakFactor += 0.0075;
    return self.breakFactor;
}


- (void)performWinnerEffect {
    self.cLabel.hidden = YES;
    self.rhLabel.hidden = YES;
    
    [self performTomatoWinnerEffect];
    [self performLabelWinnerEffect];
}


- (void)performTomatoWinnerEffect {
    void (^amplifyAnimationBlock)(void) = ^{
        self.tomatoImageView.transform = CGAffineTransformMakeScale(1.25, 1.25);
    };
    
    void (^amplifyCompletionBlock)(BOOL) = ^(BOOL finished) {
        void (^normalizeAnimationBlock)(void) = ^{
            self.tomatoImageView.transform = CGAffineTransformIdentity;
        };
        
        void (^normalizeCompletionBlock)(BOOL) = ^(BOOL finished) {
            [self performTomatoWinnerEffect];
        };
        
        [UIView animateWithDuration:0.5 animations:normalizeAnimationBlock completion:normalizeCompletionBlock];
    };
    
    [UIView animateWithDuration:0.5 animations:amplifyAnimationBlock completion:amplifyCompletionBlock];
}


- (void)performLabelWinnerEffect {
    void (^firstAnimationBlock)(void) = ^{
        self.rLabel.transform = CGAffineTransformMakeRotation(- 0.5 * M_PI_4);
    };
    
    void (^firstCompletionBlock)(BOOL) = ^(BOOL finished) {
        void (^secondAnimationBlock)(void) = ^{
            self.rLabel.transform = CGAffineTransformMakeRotation(0.5 * M_PI_4);
        };
        
        void (^secondCompletionBlock)(BOOL) = ^(BOOL finished) {
            [self performLabelWinnerEffect];
        };
        
        [UIView animateWithDuration:0.5 animations:secondAnimationBlock completion:secondCompletionBlock];
    };
    
    [UIView animateWithDuration:0.5 animations:firstAnimationBlock completion:firstCompletionBlock];
}


- (UIButton *)buttonWithTitle:(NSString *)title {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    
    button.titleLabel.font = [UIFont boldSystemFontOfSize:35.0];
    button.titleLabel.shadowColor = [UIColor blackColor];
    button.titleLabel.shadowOffset = CGSizeMake(2.0, 2.0);
    
    UIImage *buttonImage = [[UIImage imageNamed:@"greenButton.png"]
                            resizableImageWithCapInsets:UIEdgeInsetsMake(18, 18, 18, 18)];
    UIImage *buttonImageHighlight = [[UIImage imageNamed:@"greenButtonHighlight.png"]
                                     resizableImageWithCapInsets:UIEdgeInsetsMake(18, 18, 18, 18)];
    
    [button setBackgroundImage:buttonImage forState:UIControlStateNormal];
    [button setBackgroundImage:buttonImageHighlight forState:UIControlStateHighlighted];
    [button setTitle:title forState:UIControlStateNormal];
    
    return button;
}


@end
